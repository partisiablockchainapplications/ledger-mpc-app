### For Ledger Dev
```bash
git clone git@gitlab.com:partisiablockchainapplications/ledger-app.git
git checkout partisia
cd ledger-app

docker run --rm -ti -v "/dev/bus/usb:/dev/bus/usb" -v "$(realpath .):/app" --privileged ledger-app-builder:latest

# or if I dont need to load into usb
# docker run --rm -ti -v "$(realpath .):/app" ledger-app-builder:latest

make
make load
```


### For Ledger Test
* plug in ledger
* unlock it
* open Partisia App
>create `.env` file like: <br>
>MNEMONIC="...."

```bash
npm i
npm run test
```