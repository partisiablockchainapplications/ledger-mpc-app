require('dotenv').config()
import { partisiaCrypto } from 'partisia-crypto'
import MpcTransport, { EnumTrasactionPrefix } from '../src/index'
import HwTransport from '@ledgerhq/hw-transport-node-hid'

const wallet = partisiaCrypto.wallet.getKeyPairHD(process.env.MNEMONIC || '')
const BLOCKCHAIN_ID_MAINNET = 'Partisia Blockchain'
const BLOCKCHAIN_ID_TESTNET = 'Partisia Blockchain Testnet'

it('connect transport', async () => {
  const mpc = new MpcTransport(HwTransport)
  await mpc.Ready
})

it('testBadPrefix', async () => {
  const mpc = new MpcTransport(HwTransport)
  await mpc.Ready

  // build bytes
  const buf = Buffer.concat([
    Buffer.from([0x00]), // bad prefix
    Buffer.from([9, 10, 11, 12, 255]),
  ])
  console.log('size', buf.length - 1)
  await expect(mpc.signTx(buf, { debug: true })).rejects.toThrow('Ledger device: UNKNOWN_ERROR (0xb005)')
})

it('testGetPublicKey', async () => {
  const mpc = new MpcTransport(HwTransport)
  await mpc.Ready

  const res = await mpc.getAddress({ debug: true, boolDisplay: true })
  console.log(Buffer.isBuffer(res) ? res.toString('hex') : res)

  const { publicKey, address } = wallet
  expect(partisiaCrypto.wallet.getPublicKeyBuffer(publicKey, false).toString('hex')).toBe(res.publicKey)
  expect(address).toBe(res.address)
})

it('testGetAppName', async () => {
  const mpc = new MpcTransport(HwTransport)
  await mpc.Ready

  const res = await mpc.getMpcAppName({ debug: true })
  expect(res).toBe('Partisia')
})

it('testGetVersion', async () => {
  const mpc = new MpcTransport(HwTransport)
  await mpc.Ready

  const res = await mpc.getMpcVersion({ debug: true })
  expect(res).toBe('1.0.1')
})
/////////////////////
// MAINNET TESTING //
/////////////////////
it('testSignMessage', async () => {
  const mpc = new MpcTransport(HwTransport)
  await mpc.Ready

  // build bytes
  const serialized = Buffer.from('dApp Message', 'utf8') // msg bytes in UTF8
  const buf = Buffer.concat([
    Buffer.from([EnumTrasactionPrefix.SIGN_UTF8]), // sign
    serialized,
  ])

  const sigPartisia = await mpc.signTx(buf, { debug: true })
  console.log('sigPartisia', sigPartisia)

  // what the signature should be
  const digest = partisiaCrypto.transaction.deriveDigest(BLOCKCHAIN_ID_MAINNET, serialized)
  expect(partisiaCrypto.wallet.verifySignature(digest, sigPartisia, wallet.publicKey)).toBe(true)
})

it('testSignDigest', async () => {
  const mpc = new MpcTransport(HwTransport)
  await mpc.Ready

  const serialized = Buffer.from('dApp Message', 'utf8') // msg bytes in UTF8
  const digest = partisiaCrypto.transaction.deriveDigest(BLOCKCHAIN_ID_MAINNET, serialized)
  expect(digest.length).toBe(32)

  const buf = Buffer.concat([
    Buffer.from([EnumTrasactionPrefix.SIGN_DIGEST]), // sign
    digest,
  ])

  const sigPartisia = await mpc.signTx(buf, { debug: true })
  expect(partisiaCrypto.wallet.verifySignature(digest, sigPartisia, wallet.publicKey)).toBe(true)
})

it('sendTooLargePayload', async () => {
  const mpc = new MpcTransport(HwTransport)
  await mpc.Ready

  const serialized = Buffer.from(new Uint8Array(510).fill(65))

  const buf = Buffer.concat([
    Buffer.from([EnumTrasactionPrefix.SIGN_UTF8]), // sign
    serialized,
  ])

  await expect(mpc.signTx(buf, { debug: true, convertLargePayloadsToDigest: false })).rejects.toThrow(
    'Message is too large to be sent to ledger'
  )
  const sigPartisia = await mpc.signTx(buf, { debug: true })
  const digest = partisiaCrypto.transaction.deriveDigest(BLOCKCHAIN_ID_MAINNET, serialized)
  expect(partisiaCrypto.wallet.verifySignature(digest, sigPartisia, wallet.publicKey)).toBe(true)
})

it('testSignMessageLong', async () => {
  const mpc = new MpcTransport(HwTransport)
  await mpc.Ready

  // build bytes
  const serialized = Buffer.from(
    'Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Mxxxxxxxxxxx',
    'utf8'
  )
  const buf = Buffer.concat([
    Buffer.from([EnumTrasactionPrefix.SIGN_UTF8]), // sign
    // Buffer.from('00000019', 'hex'), // u32 bytes for length
    // Buffer.from('Long Message-Long Message', 'utf8'), // msg bytes in UTF8
    serialized, // msg bytes in UTF8
    // Buffer.from('0000015E', 'hex'), // u32 bytes for length
    // Buffer.from('Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message', 'utf8'), // msg bytes in UTF8
  ])
  console.log('full bytes', buf.toString('hex'))

  const sigPartisia = await mpc.signTx(buf, { debug: true })
  console.log('sigPartisia', sigPartisia)

  // what the signature should be
  const digest = partisiaCrypto.transaction.deriveDigest(BLOCKCHAIN_ID_MAINNET, serialized)
  expect(partisiaCrypto.wallet.verifySignature(digest, sigPartisia, wallet.publicKey)).toBe(true)
})

it('testSignMessageSmallChunk', async () => {
  const mpc = new MpcTransport(HwTransport)
  await mpc.Ready

  // build bytes
  const serialized = Buffer.from('hello world', 'utf8')
  const buf = Buffer.concat([
    Buffer.from([EnumTrasactionPrefix.SIGN_UTF8]), // sign
    serialized,
  ])

  const sigPartisia = await mpc.signTx(buf, { debug: true, chunkLength: 4 })
  const digest = partisiaCrypto.transaction.deriveDigest(BLOCKCHAIN_ID_MAINNET, serialized)
  expect(partisiaCrypto.wallet.verifySignature(digest, sigPartisia, wallet.publicKey)).toBe(true)
})

it('testTransferMpc', async () => {
  const mpc = new MpcTransport(HwTransport)
  await mpc.Ready

  // build bytes
  const abi = partisiaCrypto.abi_system.Payload_MpcTransfer
  const payload = partisiaCrypto.structs.serializeToBuffer(
    {
      invocationByte: 3,
      recipient: '00b48ffc5d275744b14b609f80180c44ef77992aac',
      amount: '100000',
    },
    ...abi
  )

  const serialized = partisiaCrypto.transaction.serializedTransaction(
    {
      nonce: 10,
      cost: 20,
      validTo: new Date('2022-01-01T00:00:00.000Z').toISOString(),
    },
    {
      contract: '01a4082d9d560749ecd0ffa1dcaaaee2c2cb25d881',
    },
    payload
  )
  const buf = Buffer.concat([
    Buffer.from([EnumTrasactionPrefix.SIGN_TRANSACTION]), // MPC
    serialized,
  ])

  const sigPartisia = await mpc.signTx(buf, { debug: true })

  const digest = partisiaCrypto.transaction.deriveDigest(BLOCKCHAIN_ID_MAINNET, serialized)
  expect(partisiaCrypto.wallet.verifySignature(digest, sigPartisia, wallet.publicKey)).toBe(true)
})

it('testTransferMpcWithNumberTag', async () => {
  const mpc = new MpcTransport(HwTransport)
  await mpc.Ready

  // build bytes
  const abi = partisiaCrypto.abi_system.Payload_MpcTransferWithNum
  const payload = partisiaCrypto.structs.serializeToBuffer(
    {
      invocationByte: 13,
      recipient: '00b48ffc5d275744b14b609f80180c44ef77992aac',
      amount: '100000',
      numberTag: '12345678',
    },
    ...abi
  )

  const serialized = partisiaCrypto.transaction.serializedTransaction(
    {
      nonce: 10,
      cost: 20,
      validTo: new Date('2022-01-01T00:00:00.000Z').toISOString(),
    },
    {
      contract: '01a4082d9d560749ecd0ffa1dcaaaee2c2cb25d881',
    },
    payload
  )
  const buf = Buffer.concat([
    Buffer.from([EnumTrasactionPrefix.SIGN_TRANSACTION]), // MPC
    serialized,
  ])

  const sigPartisia = await mpc.signTx(buf, { debug: true })

  const digest = partisiaCrypto.transaction.deriveDigest(BLOCKCHAIN_ID_MAINNET, serialized)
  expect(partisiaCrypto.wallet.verifySignature(digest, sigPartisia, wallet.publicKey)).toBe(true)
})

it('testTransferMpcWithMemo', async () => {
  const mpc = new MpcTransport(HwTransport)
  await mpc.Ready

  // build bytes
  const abi = partisiaCrypto.abi_system.Payload_MpcTransferWithMemo
  const payload = partisiaCrypto.structs.serializeToBuffer(
    {
      invocationByte: 23,
      recipient: '00b48ffc5d275744b14b609f80180c44ef77992aac',
      amount: '100000',
      memo: 'memo here',
    },
    ...abi
  )

  const serialized = partisiaCrypto.transaction.serializedTransaction(
    {
      nonce: 10,
      cost: 20,
      validTo: new Date('2022-01-01T00:00:00.000Z').toISOString(),
    },
    {
      contract: '01a4082d9d560749ecd0ffa1dcaaaee2c2cb25d881',
    },
    payload
  )
  const buf = Buffer.concat([
    Buffer.from([EnumTrasactionPrefix.SIGN_TRANSACTION]), // MPC
    serialized,
  ])

  const sigPartisia = await mpc.signTx(buf, { debug: true })

  const digest = partisiaCrypto.transaction.deriveDigest(BLOCKCHAIN_ID_MAINNET, serialized)
  expect(partisiaCrypto.wallet.verifySignature(digest, sigPartisia, wallet.publicKey)).toBe(true)
})

it('testContractStake', async () => {
  const mpc = new MpcTransport(HwTransport)
  await mpc.Ready

  // build bytes
  const abi = partisiaCrypto.abi_system.Payload_StakeTokens
  const payload = partisiaCrypto.structs.serializeToBuffer(
    {
      invocationByte: 10,
      amount: '10000',
    },
    ...abi
  )

  const serialized = partisiaCrypto.transaction.serializedTransaction(
    {
      nonce: 10,
      cost: 20,
      validTo: new Date('2022-01-01T00:00:00.000Z').toISOString(),
    },
    {
      contract: '01a2020bb33ef9e0323c7a3210d5cb7fd492aa0d65',
    },
    payload
  )
  const buf = Buffer.concat([
    Buffer.from([EnumTrasactionPrefix.SIGN_TRANSACTION]), // public
    serialized,
  ])

  const sigPartisia = await mpc.signTx(buf, { debug: true })

  const digest = partisiaCrypto.transaction.deriveDigest(BLOCKCHAIN_ID_MAINNET, serialized)
  expect(partisiaCrypto.wallet.verifySignature(digest, sigPartisia, wallet.publicKey)).toBe(true)
})

/////////////////////
// TESTNET TESTING //
/////////////////////
it('testSignMessageTestnet', async () => {
  const mpc = new MpcTransport(HwTransport)
  await mpc.Ready

  // build bytes
  const serialized = Buffer.from('dApp Message', 'utf8') // msg bytes in UTF8
  const buf = Buffer.concat([
    Buffer.from([EnumTrasactionPrefix.SIGN_UTF8_TESTNET]), // sign
    serialized,
  ])

  const sigPartisia = await mpc.signTx(buf, { debug: true })
  console.log('sigPartisia', sigPartisia)

  // what the signature should be
  const digest = partisiaCrypto.transaction.deriveDigest(BLOCKCHAIN_ID_TESTNET, serialized)
  expect(partisiaCrypto.wallet.verifySignature(digest, sigPartisia, wallet.publicKey)).toBe(true)
})

it('testSignDigestTestnet', async () => {
  const mpc = new MpcTransport(HwTransport)
  await mpc.Ready

  const serialized = Buffer.from('dApp Message', 'utf8') // msg bytes in UTF8
  const digest = partisiaCrypto.transaction.deriveDigest(BLOCKCHAIN_ID_TESTNET, serialized)
  expect(digest.length).toBe(32)

  const buf = Buffer.concat([
    Buffer.from([EnumTrasactionPrefix.SIGN_DIGEST]), // sign
    digest,
  ])

  const sigPartisia = await mpc.signTx(buf, { debug: true })
  expect(partisiaCrypto.wallet.verifySignature(digest, sigPartisia, wallet.publicKey)).toBe(true)
})
it('sendTooLargePayloadTestnet', async () => {
  const mpc = new MpcTransport(HwTransport)
  await mpc.Ready

  const serialized = Buffer.from(new Uint8Array(510).fill(65))

  const buf = Buffer.concat([
    Buffer.from([EnumTrasactionPrefix.SIGN_UTF8_TESTNET]), // sign
    serialized,
  ])

  await expect(mpc.signTx(buf, { debug: true, convertLargePayloadsToDigest: false })).rejects.toThrow(
    'Message is too large to be sent to ledger'
  )
  const sigPartisia = await mpc.signTx(buf, { debug: true })
  const digest = partisiaCrypto.transaction.deriveDigest(BLOCKCHAIN_ID_TESTNET, serialized)
  expect(partisiaCrypto.wallet.verifySignature(digest, sigPartisia, wallet.publicKey)).toBe(true)
})

it('testSignMessageLong', async () => {
  const mpc = new MpcTransport(HwTransport)
  await mpc.Ready

  // build bytes
  const serialized = Buffer.from(
    'Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Mxxxxxxxxxxx',
    'utf8'
  )
  const buf = Buffer.concat([
    Buffer.from([EnumTrasactionPrefix.SIGN_UTF8_TESTNET]), // sign
    // Buffer.from('00000019', 'hex'), // u32 bytes for length
    // Buffer.from('Long Message-Long Message', 'utf8'), // msg bytes in UTF8
    serialized, // msg bytes in UTF8
    // Buffer.from('0000015E', 'hex'), // u32 bytes for length
    // Buffer.from('Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message-Long Message', 'utf8'), // msg bytes in UTF8
  ])
  console.log('full bytes', buf.toString('hex'))

  const sigPartisia = await mpc.signTx(buf, { debug: true })
  console.log('sigPartisia', sigPartisia)

  // what the signature should be
  const digest = partisiaCrypto.transaction.deriveDigest(BLOCKCHAIN_ID_TESTNET, serialized)
  expect(partisiaCrypto.wallet.verifySignature(digest, sigPartisia, wallet.publicKey)).toBe(true)
})

it('testSignMessageSmallChunk', async () => {
  const mpc = new MpcTransport(HwTransport)
  await mpc.Ready

  // build bytes
  const serialized = Buffer.from('hello world', 'utf8')
  const buf = Buffer.concat([
    Buffer.from([EnumTrasactionPrefix.SIGN_UTF8_TESTNET]), // sign
    serialized,
  ])

  const sigPartisia = await mpc.signTx(buf, { debug: true, chunkLength: 4 })
  const digest = partisiaCrypto.transaction.deriveDigest(BLOCKCHAIN_ID_TESTNET, serialized)
  expect(partisiaCrypto.wallet.verifySignature(digest, sigPartisia, wallet.publicKey)).toBe(true)
})

it('testTransferMpc', async () => {
  const mpc = new MpcTransport(HwTransport)
  await mpc.Ready

  // build bytes
  const abi = partisiaCrypto.abi_system.Payload_MpcTransfer
  const payload = partisiaCrypto.structs.serializeToBuffer(
    {
      invocationByte: 3,
      recipient: '00b48ffc5d275744b14b609f80180c44ef77992aac',
      amount: '100000',
    },
    ...abi
  )

  const serialized = partisiaCrypto.transaction.serializedTransaction(
    {
      nonce: 10,
      cost: 20,
      validTo: new Date('2022-01-01T00:00:00.000Z').toISOString(),
    },
    {
      contract: '01a4082d9d560749ecd0ffa1dcaaaee2c2cb25d881',
    },
    payload
  )
  const buf = Buffer.concat([
    Buffer.from([EnumTrasactionPrefix.SIGN_TRANSACTION_TESTNET]), // MPC
    serialized,
  ])

  const sigPartisia = await mpc.signTx(buf, { debug: true })

  const digest = partisiaCrypto.transaction.deriveDigest(BLOCKCHAIN_ID_TESTNET, serialized)
  expect(partisiaCrypto.wallet.verifySignature(digest, sigPartisia, wallet.publicKey)).toBe(true)
})

it('testTransferMpcWithNumberTag', async () => {
  const mpc = new MpcTransport(HwTransport)
  await mpc.Ready

  // build bytes
  const abi = partisiaCrypto.abi_system.Payload_MpcTransferWithNum
  const payload = partisiaCrypto.structs.serializeToBuffer(
    {
      invocationByte: 13,
      recipient: '00b48ffc5d275744b14b609f80180c44ef77992aac',
      amount: '100000',
      numberTag: '12345678',
    },
    ...abi
  )

  const serialized = partisiaCrypto.transaction.serializedTransaction(
    {
      nonce: 10,
      cost: 20,
      validTo: new Date('2022-01-01T00:00:00.000Z').toISOString(),
    },
    {
      contract: '01a4082d9d560749ecd0ffa1dcaaaee2c2cb25d881',
    },
    payload
  )
  const buf = Buffer.concat([
    Buffer.from([EnumTrasactionPrefix.SIGN_TRANSACTION_TESTNET]), // MPC
    serialized,
  ])

  const sigPartisia = await mpc.signTx(buf, { debug: true })

  const digest = partisiaCrypto.transaction.deriveDigest(BLOCKCHAIN_ID_TESTNET, serialized)
  expect(partisiaCrypto.wallet.verifySignature(digest, sigPartisia, wallet.publicKey)).toBe(true)
})

it('testTransferMpcWithMemo', async () => {
  const mpc = new MpcTransport(HwTransport)
  await mpc.Ready

  // build bytes
  const abi = partisiaCrypto.abi_system.Payload_MpcTransferWithMemo
  const payload = partisiaCrypto.structs.serializeToBuffer(
    {
      invocationByte: 23,
      recipient: '00b48ffc5d275744b14b609f80180c44ef77992aac',
      amount: '100000',
      memo: 'memo here',
    },
    ...abi
  )

  const serialized = partisiaCrypto.transaction.serializedTransaction(
    {
      nonce: 10,
      cost: 20,
      validTo: new Date('2022-01-01T00:00:00.000Z').toISOString(),
    },
    {
      contract: '01a4082d9d560749ecd0ffa1dcaaaee2c2cb25d881',
    },
    payload
  )
  const buf = Buffer.concat([
    Buffer.from([EnumTrasactionPrefix.SIGN_TRANSACTION_TESTNET]), // MPC
    serialized,
  ])

  const sigPartisia = await mpc.signTx(buf, { debug: true })

  const digest = partisiaCrypto.transaction.deriveDigest(BLOCKCHAIN_ID_TESTNET, serialized)
  expect(partisiaCrypto.wallet.verifySignature(digest, sigPartisia, wallet.publicKey)).toBe(true)
})

it('testContractStake', async () => {
  const mpc = new MpcTransport(HwTransport)
  await mpc.Ready

  // build bytes
  const abi = partisiaCrypto.abi_system.Payload_StakeTokens
  const payload = partisiaCrypto.structs.serializeToBuffer(
    {
      invocationByte: 10,
      amount: '10000',
    },
    ...abi
  )

  const serialized = partisiaCrypto.transaction.serializedTransaction(
    {
      nonce: 10,
      cost: 20,
      validTo: new Date('2022-01-01T00:00:00.000Z').toISOString(),
    },
    {
      contract: '01a2020bb33ef9e0323c7a3210d5cb7fd492aa0d65',
    },
    payload
  )
  const buf = Buffer.concat([
    Buffer.from([EnumTrasactionPrefix.SIGN_TRANSACTION_TESTNET]), // public
    serialized,
  ])

  const sigPartisia = await mpc.signTx(buf, { debug: true })

  const digest = partisiaCrypto.transaction.deriveDigest(BLOCKCHAIN_ID_TESTNET, serialized)
  expect(partisiaCrypto.wallet.verifySignature(digest, sigPartisia, wallet.publicKey)).toBe(true)
})
