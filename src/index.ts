import { MpcTransport } from './main/mpc_transport'

export default MpcTransport

export const EnumTrasactionPrefix = Object.freeze({
  // mainnet
  SIGN_UTF8: 0x01,
  SIGN_TRANSACTION: 0x02,
  SIGN_DIGEST: 0x03,

  // testnet
  SIGN_UTF8_TESTNET: 0x0B,
  SIGN_TRANSACTION_TESTNET: 0x0C,
})

export const MAX_TRANSACTION_LEN = 510
