import Transport from '@ledgerhq/hw-transport'
import assert from 'assert'
import { deriveDigest, derivePartisiaSignature, publicKeyToAddress, splitPath } from './utils'
import { ec as Elliptic } from 'elliptic'
import { StatusWords } from './status_words'
import { EnumTrasactionPrefix, MAX_TRANSACTION_LEN } from '..'
const ec = new Elliptic('secp256k1')

// /home/linux/git/ledger-app-boilerplate/src/types.h
// /**
//  * Structure with fields of APDU command.
//  */
//  typedef struct {
//     uint8_t cla;    /// Instruction class
//     command_e ins;  /// Instruction code
//     uint8_t p1;     /// Instruction parameter 1
//     uint8_t p2;     /// Instruction parameter 2
//     uint8_t lc;     /// Length of command data
//     uint8_t *data;  /// Command data
// } command_t;

// /home/linux/git/ledger-app-boilerplate/doc/COMMANDS.md
enum InsType {
  INS_GET_VERSION = 0x03,
  INS_GET_APP_NAME = 0x04,
  INS_GET_PUBLIC_KEY = 0x05,
  INS_SIGN_TX = 0x06,
}

// this is the base path
const PathHD = `44'/3757'/0'/0`

const assertStatus = (response: Buffer) => {
  assert(response.length >= 2, 'no readable response')
  const status = parseInt(response.subarray(-2).toString('hex'), 16)
  const msg = StatusWords[status]
  assert(status === 0x9000, msg)
}

export class MpcTransport {
  public Ready: Promise<any>
  private transport: Transport
  constructor(HwTransport: any) {
    this.Ready = new Promise((resolve, reject) => {
      HwTransport.create()
        .then(res => {
          this.transport = res
          resolve(undefined)
        })
        .catch(reject)
    })
  }
  async getAddress(args?: { path?: string; boolDisplay?: boolean; boolChaincode?: boolean; debug?: boolean }) {
    const { path, boolDisplay, boolChaincode, debug } = {
      path: PathHD + '/0',
      ...(args ?? {}),
    }

    const paths = splitPath(path)

    const data = Buffer.alloc(1 + paths.length * 4)
    data[0] = paths.length
    paths.forEach((element, index) => {
      data.writeUInt32BE(element, 1 + 4 * index)
    })

    // /home/linux/git/ledger-app-boilerplate/tests/ledgercomm/boilerplate_client/boilerplate_cmd_builder.py
    const p1 = boolDisplay ? 0x01 : 0x00
    const p2 = 0x00

    const cla = 0xe0
    const ins = InsType.INS_GET_PUBLIC_KEY
    if (debug) console.log('sending bytes', JSON.stringify({ p1, p2, cla, ins, data: data.toString('hex') }, null, 2))

    // This will throw an error on anything other then 0x9000
    const response = await this.transport.send(cla, ins, p1, p2, data)
    // assertStatus(response)
    if (debug) console.log('response', response.toString('hex'))

    // first byte is pubkey length
    const pubKeyLen = response[0]
    const publicKeyRes = response.subarray(1, pubKeyLen + 1)
    const publicKey = Buffer.from(ec.keyFromPublic(publicKeyRes).getPublic(false, 'array'))

    // first byte of publicKey should always be 0x04
    assert(publicKey[0] === 0x04)
    assert(publicKey.length === 65)

    const chaincodeLen = response.subarray(pubKeyLen + 1, pubKeyLen + 2)[0]
    const chainCode = response.subarray(pubKeyLen + 2, pubKeyLen + 2 + chaincodeLen)

    const SW_OK = response.subarray(pubKeyLen + 2 + chaincodeLen, pubKeyLen + 2 + chaincodeLen + 2)
    if (debug) console.log('SW_OK', SW_OK.toString('hex'))

    assert(SW_OK.equals(Buffer.from([0x90, 0x00])))
    assert(response.length === pubKeyLen + 2 + chaincodeLen + 2)

    return {
      publicKey: publicKey.toString('hex'),
      chainCode: chainCode.toString('hex'),
      address: publicKeyToAddress(publicKey),
    }
  }
  async getMpcAppName(args?: { debug?: boolean }) {
    const { debug } = { ...(args ?? {}) }

    const p1 = 0x00
    const p2 = 0x00
    const cla = 0xe0
    const ins = InsType.INS_GET_APP_NAME
    const data = Buffer.from([])

    if (debug) console.log('sending bytes', JSON.stringify({ p1, p2, cla, ins, data: data.toString('hex') }, null, 2))

    const response = await this.transport.send(cla, ins, p1, p2, data)
    if (debug) console.log('response', response.toString('hex'))
    assertStatus(response)

    assert(response.length > 2)
    const SW_OK = response.subarray(-2)
    assert(SW_OK.equals(Buffer.from([0x90, 0x00])))

    return response.subarray(0, response.length - 2).toString('utf8')
  }
  async getMpcVersion(args?: { debug?: boolean }) {
    const { debug } = { ...(args ?? {}) }

    const p1 = 0x00
    const p2 = 0x00
    const cla = 0xe0
    const ins = InsType.INS_GET_VERSION
    const data = Buffer.from([])

    if (debug) console.log('sending bytes', JSON.stringify({ p1, p2, cla, ins, data: data.toString('hex') }, null, 2))

    const response = await this.transport.send(cla, ins, p1, p2, data)
    if (debug) console.log('response', response.toString('hex'))
    assertStatus(response)

    assert(response.length === 5)
    const SW_OK = response.subarray(-2)
    assert(SW_OK.equals(Buffer.from([0x90, 0x00])))

    const major = response[0]
    const minor = response[1]
    const patch = response[2]

    return [major, minor, patch].join('.')
  }

  // SIGN_TX
  async signTx(
    tx: Buffer,
    args?: {
      chunkLength?: number
      path?: string
      debug?: boolean
      convertLargePayloadsToDigest?: boolean
    }
  ): Promise<string> {
    const { path, debug, chunkLength, convertLargePayloadsToDigest } = {
      path: PathHD + '/0',
      chunkLength: 255,
      convertLargePayloadsToDigest: true,
      ...(args ?? {}),
    }

    var p1 = 0x00 // chunk index
    var p2 = 0x80 // 0x00 (more) | 0x80 (last)
    const cla = 0xe0
    const ins = InsType.INS_SIGN_TX

    const paths = splitPath(path)

    const dataPathHD = Buffer.alloc(1 + paths.length * 4)
    dataPathHD[0] = paths.length
    paths.forEach((element, index) => {
      dataPathHD.writeUInt32BE(element, 1 + 4 * index)
    })

    // send the address path
    if (debug)
      console.log(
        'sending bytes',
        JSON.stringify(
          {
            header: Buffer.from([cla, ins, p1, p2]).toString('hex'),
            data: dataPathHD.toString('hex'),
          },
          null,
          2
        )
      )

    var response: Buffer = Buffer.from([])

    // send the address path
    await this.transport.send(cla, ins, p1, p2, dataPathHD)

    if (tx.length >= MAX_TRANSACTION_LEN + 1 && convertLargePayloadsToDigest) {
      const isTestnet = tx[0] > 10
      const payload = tx.subarray(1)
      const digest = deriveDigest(payload, isTestnet)
      tx = Buffer.concat([Buffer.from([EnumTrasactionPrefix.SIGN_DIGEST]), digest])
    }
    assert(tx.length <= MAX_TRANSACTION_LEN, 'Message is too large to be sent to ledger')
    const lenChunks = Math.floor(tx.length / chunkLength) + (tx.length % chunkLength === 0 ? 0 : 1)
    assert(lenChunks <= 3, 'Message is too large to be sent to ledger')
    if (debug) console.log('chunks size', lenChunks)
    for (let i = 0; i < lenChunks; i++) {
      const data = tx.subarray(i * chunkLength, (i + 1) * chunkLength)
      const isLast = i + 1 === lenChunks

      p1 = i + 1
      p2 = isLast ? 0x00 : 0x80

      if (debug)
        console.log(
          'sending bytes',
          JSON.stringify(
            {
              header: Buffer.from([cla, ins, p1, p2]).toString('hex'),
              data: data.toString('hex'),
            },
            null,
            2
          )
        )

      if (isLast) {
        response = await this.transport.send(cla, ins, p1, p2, data)
      } else {
        await this.transport.send(cla, ins, p1, p2, data)
      }
    }
    if (debug) console.log('response', response.toString('hex'))
    assertStatus(response)

    const sigLength = response[0]
    const sig = response.subarray(1, sigLength + 1)
    const v = response.subarray(sigLength + 1, sigLength + 2)[0]
    const SW_OK = response.subarray(sigLength + 2, sigLength + 4)
    assert(SW_OK.equals(Buffer.from([0x90, 0x00])))
    assert(response.length === sigLength + 4)

    const sigPartisia = derivePartisiaSignature(sig, v)

    return sigPartisia.toString('hex')
    // return {
    //     sigDer: sig.toString('hex'),
    //     sigPartisia: sigPartisia.toString('hex'),
    //     sig: {
    //         recoveryId: v,
    //         r: sigPartisia.subarray(1, 33).toString('hex'),
    //         s: sigPartisia.subarray(33, 65).toString('hex'),
    //     }
    // }
  }
}
