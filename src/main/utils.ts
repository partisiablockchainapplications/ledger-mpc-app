import assert from 'assert'
import crypto from 'crypto' // This is node.js library https://nodejs.org/api/crypto.html
import { derToJose, joseToDer } from 'ecdsa-sig-formatter'

export function splitPath(path: string): number[] {
  const result: number[] = []
  const components = path.split('/')
  components.forEach(element => {
    let number = parseInt(element, 10)
    if (isNaN(number)) {
      return // FIXME shouldn't it throws instead?
    }
    if (element.length > 1 && element[element.length - 1] === "'") {
      number += 0x80000000
    }
    result.push(number)
  })
  return result
}

export function publicKeyToAddress(publicKey: Buffer): string {
  assert(publicKey.length === 65)
  const hash = crypto
    .createHash('sha256')
    .update(publicKey)
    .digest()
  return '00' + hash.toString('hex').substring(24)
}

export function derivePartisiaSignature(sigDer: Buffer, v: number): Buffer {
  // decode the signature
  // https://stackoverflow.com/a/41730518/7959616
  // first byte is 0x30
  // assert(sig[0] === 0x30, 'malformed signature')
  // // second byte is length
  // const l = sig[1]
  // assert(l + 2 == sig.length)
  // // third byte is header length
  // assert(sig[2] === 0x02)
  // // fourth byte is len R
  // const lenR = sig[3]
  // const R = sig.subarray(4, 4 + lenR)

  // assert(sig[4 + lenR] === 0x02)
  // const lenS = sig[5 + lenR]
  // const S = sig.subarray(6 + lenR, 6 + lenR + lenS)

  // assert([0, 1, 2, 3].includes(v), `recovery id must be between 0-3 inclusive but received ${v}`)
  // assert(lenR <= 33, 'lenR must be lte 33')
  // assert(lenS <= 33, 'lenS must be lte 33')

  // assert(6 + lenR + lenS === sigLength)
  const sigJose = Buffer.from(derToJose(sigDer, 'ES256'), 'base64')
  // const sigDer = joseToDer(sigJose, 'ES256')
  // assert(sigDer.equals(sigDer), 'mismatch signatures DER and JOSE')

  const sigPartisia = Buffer.concat([Buffer.from([v]), sigJose])
  assert(sigPartisia.length === 65, 'sigPartisia mismatch length')
  return sigPartisia
}
export function hashBuffers(buffers: Buffer[]): Buffer {
  const hash = crypto.createHash('sha256')

  for (const buffer of buffers) {
    hash.update(buffer)
  }

  return Buffer.from(hash.digest())
}
export function deriveDigest(serializedTransaction: Buffer, isTestnet: boolean) {
  // this is the utf8 buffer of utf8 "Partisia Blockchain"
  // prettier-ignore
  const chainId = isTestnet
    ? Buffer.from([0, 0, 0, 27, 80, 97, 114, 116, 105, 115, 105, 97, 32, 66, 108, 111, 99, 107, 99, 104, 97, 105, 110, 32, 84, 101, 115, 116, 110, 101, 116])
    : Buffer.from([0, 0, 0, 19, 80, 97, 114, 116, 105, 115, 105, 97, 32, 66, 108, 111, 99, 107, 99, 104, 97, 105, 110])
  const digest = hashBuffers([serializedTransaction, chainId])
  return digest
}
